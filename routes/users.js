var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/user.js');
var Controller = require('../models/controller.js');
var Patient = require('../models/patient.js');
var Jar = require('../models/jar.js');
var auth = require('../auth.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  User.findAsync().then(function(users) {
    return res.json(users);
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

router.get('/:id', function(req, res, next) {
  User.findOne({
      userId: req.params.id
    }).lean().execAsync()
    .then(function(user) {
      if (user == null) {
        return res.status(404).json({
          error: 'Not found for user id'
        });
      }
      Controller.findAsync({
        careTaker: user.userId
      }).then(function(controllers) {
        user.controllers = controllers;
        Patient.find({
            careTakerId: user.userId
          }).lean().execAsync()
          .then(function(patients) {
            var total = patients.length;
            var i = 0;
            patients.forEach(function(item) {
              console.log(item);
              item.jars = [];
              Jar.findAsync({
                patientId: item.patientId
              }).then(function(jars){
                item.jars = jars;
                i++;
                if (i == total) {
                  user.patients = patients;
                  return res.json(user);
                }
              })
            });
          }).catch(function(error) {
            console.error(error);
            return res.json(user);
          });
      });
    }).catch(function(err) {
      console.error(err);
    });
});

router.post('/', function(req, res, next) {
  User.findOneAsync({
    email: req.body.email
  }).then(function(user) {
    if (user != null) {
      return res.json(user);
    } else {
      User.createAsync(req.body).then(function(post) {
        return res.json(post);
      });
    }
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

module.exports = router;

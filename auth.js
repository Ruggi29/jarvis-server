var Token = require('./models/token')
var express = require('express');
module.exports = {
  authenticate: function() {
    return function(req, res, next) {
      P.onPossiblyUnhandledRejection(function(error) {
        res.status(403).json({
          error: 'login is required to access'
        });
      });

      if (!req.query.token)
        P.reject();
      else {
        Token.findAsync({
          value: req.query.token
        }).then(function(token) {
          if (token.length == 0)
            P.reject();
          else
            next();
        }).catch(function(err) {
          P.reject()
        });
      }
    }
  }
}

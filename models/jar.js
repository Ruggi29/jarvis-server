var mongoose = require('mongoose');
var medicine = require('../data/medicine.json');
var aI = require('mongoose-auto-increment');

var JarSchema = new mongoose.Schema({
  name: String,
  uuid: String,
  weight: Number,
  maxWeight: Number,
  trigger: Number,
  patientId: Number,
  created_at: {
    type: Date,
    default: Date.now
  },
  controllerId: Number,
  updated_at: Date,
  medicine: {}
});

aI.initialize(mongoose.connection);
JarSchema.plugin(aI.plugin, {
  model: 'Jar',
  field: 'jarId'
});


JarSchema.pre('save', function(next) {
  this.updated_at = new Date();
  // var medicineName = medicine[0];
  // for (var i = 0; i < medicine.length; i++) {
  //   if (this.name != undefined && medicine[i].medicine.toUpperCase() === this.name.toUpperCase()) {
  //     medicineName = medicine[i];
  //   }
  // }
  // this.medicine = medicineName;
  next();
});

module.exports = mongoose.model('Jar', JarSchema);

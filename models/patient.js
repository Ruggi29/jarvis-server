var mongoose = require('mongoose');
var aI = require('mongoose-auto-increment');

var PatientSchema = new mongoose.Schema({
  name: String,
  careTakerId: Number,
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: Date
});

aI.initialize(mongoose.connection);
PatientSchema.plugin(aI.plugin, {
  model: 'Patient',
  field: 'patientId',
  startAt: 1
});


PatientSchema.pre('save', function(next) {
  this.updated_at = new Date();
  next();
});

module.exports = mongoose.model('Patient', PatientSchema);
